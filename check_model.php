<?php

class check_base {

	public $P1;
	public $P2;
	public $P3;
	public $P4;

}

function f_check_model($i_model) {

	$provjera1 = new check_base;
	$provjera2 = new check_base;
	$provjera3 = new check_base;
	$provjera4 = new check_base;
	$provjera5 = new check_base;
	$provjera6 = new check_base;
	$provjera7 = new check_base;
	$provjera8 = new check_base;
	$provjera9 = new check_base;
	$provjera10 = new check_base;
	$provjera11 = new check_base;
	$provjera12 = new check_base;
	$provjera13 = new check_base;
	$provjera14 = new check_base;
	$provjera15 = new check_base;
	$provjera16 = new check_base;
	$provjera17 = new check_base;
	$provjera18 = new check_base;
	$provjera19 = new check_base;
	$provjera20 = new check_base;
	$provjera21 = new check_base;
	$provjera22 = new check_base;
	$provjera23 = new check_base;
	$provjera24 = new check_base;
	$provjera25 = new check_base;
	$provjera26 = new check_base;
	$provjera27 = new check_base;
	$provjera28 = new check_base;
	$provjera29 = new check_base;
	$provjera30 = new check_base;
	$provjera31 = new check_base;


	switch ($i_model) {
	
	case "HR00":
	case "HR01":
	case "HR02":
	case "HR03":
	case "HR04":
	case "HR05":
	case "HR06":
	case "HR07":
	case "HR08":
	case "HR09":
	case "HR10":
	case "HR17":
	case "HR18":
	case "HR11":
	case "HR42":
	case "HR55":
		$provjera1->P1 = 12;
		$provjera1->P2 = 12;
		$provjera1->P3 = 12;
		$provjera1->P4 = NULL;
		//echo $provjera1->P1;
		return $provjera1;
		
	case "HR41":
	case "HR12":
		$provjera2->P1 = 13;
		$provjera2->P2 = 12;
		$provjera2->P3 = 12;
		$provjera2->P4 = NULL;
		return $provjera2;

	case "HR14":
	case "HR13":

		$provjera3->P1 = 10;
		$provjera3->P2 = 12;
		$provjera3->P3 = 12;
		$provjera3->P4 = NULL;
		return $provjera3;

	case "HR15":
		$provjera4->P1 = 8;
		$provjera4->P2 = 11;
		$provjera4->P3 = NULL;
		$provjera4->P4 = NULL;
		return $provjera4;

	case "HR16":
		$provjera5->P1 = 5;
		$provjera5->P2 = 4;
		$provjera5->P3 = 8;
		$provjera5->P4 = NULL;
		return $provjera5;
	
	case "HR23":
		$provjera6->P1 = 4;
		$provjera6->P2 = 12;
		$provjera6->P3 = 12;
		$provjera6->P4 = 12;
		return $provjera6;

	case "HR24":
		$provjera7->P1 = 4;
		$provjera7->P2 = 13;
		$provjera7->P3 = 12;
		$provjera7->P4 = 12;
		return $provjera7;

	case "HR25":
		$provjera8->P1 = 3;
		$provjera8->P2 = 7;
		$provjera8->P3 = NULL;
		$provjera8->P4 = NULL;
		return $provjera8;

	case "HR26":
		$provjera9->P1 = 4;
		$provjera9->P2 = 11;
		$provjera9->P3 = 11;
		$provjera9->P4 = 12;
		return $provjera9;

	case "HR27":
		$provjera10->P1 = 4;
		$provjera10->P2 = 12;
		$provjera10->P3 = NULL;
		$provjera10->P4 = NULL;
		return $provjera10;

	case "HR28":
		$provjera11->P1 = 4;
		$provjera11->P2 = 3;
		$provjera11->P3 = 6;
		$provjera11->P4 = 6;
		return $provjera11;

	case "HR29":
		$provjera12->P1 = 4;
		$provjera12->P2 = 12;
		$provjera12->P3 = 12;
		$provjera12->P4 = NULL;
		return $provjera12;

	case "HR30":
		$provjera13->P1 = 10;
		$provjera13->P2 = 4;
		$provjera13->P3 = 6;
		$provjera13->P4 = NULL;
		return $provjera13;

	case "HR31":
		$provjera14->P1 = 6;
		$provjera14->P2 = 12;
		$provjera14->P3 = 12;
		$provjera14->P4 = 12;
		return $provjera14;

	case "HR33":
		$provjera15->P1 = 6;
		$provjera15->P2 = 7;
		$provjera15->P3 = 7;
		$provjera15->P4 = NULL;
		return $provjera15;

	case "HR34":
		$provjera16->P1 = 6;
		$provjera16->P2 = 7;
		$provjera16->P3 = 5;
		$provjera16->P4 = NULL;
		return $provjera16;

	case "HR35":
		$provjera17->P1 = 10;
		$provjera17->P2 = 11;
		$provjera17->P3 = NULL;
		$provjera17->P4 = NULL;
		return $provjera17;

	case "HR40":
		$provjera18->P1 = 11;
		$provjera18->P2 = 12;
		$provjera18->P3 = 12;
		$provjera18->P4 = NULL;
		return $provjera18;

	case "HR43":
		$provjera19->P1 = 3;
		$provjera19->P2 = 8;
		$provjera19->P3 = 5;
		$provjera19->P4 = 3;
		return $provjera19;

	case "HR50":
		$provjera20->P1 = 5;
		$provjera20->P2 = 12;
		$provjera20->P3 = 1;
		$provjera20->P4 = NULL;
		return $provjera20;

	case "HR62":
		$provjera21->P1 = 4;
		$provjera21->P2 = 5;
		$provjera21->P3 = 6;
		$provjera21->P4 = 12;
		return $provjera21;

	case "HR63":
		$provjera22->P1 = 4;
		$provjera22->P2 = 5;
		$provjera22->P3 = 12;
		$provjera22->P4 = NULL;
		return $provjera22;

	case "HR64":
		$provjera23->P1 = 4;
		$provjera23->P2 = 5;
		$provjera23->P3 = 12;
		$provjera23->P4 = 9;
		return $provjera23;

	case "HR65":
		$provjera24->P1 = 4;
		$provjera24->P2 = 3;
		$provjera24->P3 = 11;
		$provjera24->P4 = 12;
		return $provjera24;

	case "HR67":
		$provjera25->P1 = 11;
		$provjera25->P2 = 10;
		$provjera25->P3 = 8;
		$provjera25->P4 = NULL;
		return $provjera25;

	case "HR68":
		$provjera26->P1 = 4;
		$provjera26->P2 = 11;
		$provjera26->P3 = 5;
		$provjera26->P4 = NULL;
		return $provjera26;

	case "HR69":
		$provjera27->P1 = 5;
		$provjera27->P2 = 11;
		$provjera27->P3 = 3;
		$provjera27->P4 = NULL;
		return $provjera27;

	case "HR83":
		$provjera28->P1 = 4;
		$provjera28->P2 = 16;
		$provjera28->P3 = 6;
		$provjera28->P4 = NULL;
		return $provjera28;

	case "HR84":
		$provjera29->P1 = 4;
		$provjera29->P2 = 8;
		$provjera29->P3 = 10;
		$provjera29->P4 = NULL;
		return $provjera29;

	case "HR99":
		$provjera30->P1 = NULL;
		$provjera30->P2 = NULL;
		$provjera30->P3 = NULL;
		$provjera30->P4 = NULL;
		return $provjera30;

	default:
		echo "Unijeli ste nepostojeci model";
		echo "<br>";
		echo "Pritisnite bilo koju tipku za ponovni pokusaj ili tipku ESC za izlaz";
		break;
	
	
	}
	
	$provjera31->P1 = NULL;
	$provjera31->P2 = NULL;
	$provjera31->P3 = NULL;
	$provjera31->P4 = NULL;

	return $provjera31;
}

f_check_model("HR00");

?>