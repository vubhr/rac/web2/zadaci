<?php
//-------------------------------------------------------------------
//deklaracija klasa
  class Person {
     //klasa može sadržavati svoje varijable koje nazivamo properties ili atributes ili instance variable
     //ovako se deklariraju mora ići ključna riječ var
     var $first_name;
     var $last_name;
     var $arm_count = 2;
     var $leg_count = 2;
  
     //klasa može sadržavati metode koje se izvan klase nazivaju funkcije
     function say_hello(){
        echo "<br>Bok iz klase " . get_class($this);
     }
	 
     //nova metoda
     function hello(){
        $this->say_hello();
     }
     
     function full_name(){
        return $this->first_name . " " . $this->last_name; 
     }
  }
//-------------------------------------------------------------------  
//nasljeđivanje klasa
  class Auto{
     var $kotači = 4;
     var $vrata = 4;
     
     function ukupanBroj(){
        return $this->kotači + $this->vrata;
     
     }
  }
  
  //Električni nasljeđuje sve što ima i klasa Auto s tim da te neke vrijednosti možemo override-ati 
  //što se može vidjeti u primjeru
  class Električni extends Auto{
        var $vrata = 5;
		var $baterija = 1;
        function ukupanBroj(){
           return $this->kotači + $this->vrata + $this->baterija;
     
     }
  }
  
//---------------------------------------------------------------------
//Pristupi access modifiers
class Example {
   public    $a=1;
   protected $b=2;
   private   $c=3;
   
   function show_abc(){
      echo $this->a;
      echo $this->b;
      echo $this->c;
   }
   
   public function hello_everyone(){
     return "Hello everyone.<br>";
   }
   
   protected function hello_family(){
     return "Hello family.<br>";
   }

   public function hello_me(){
     return "Hello me.<br>";
   }
   
   function hello(){
      $output = $this->hello_everyone();
      $output .= $this->hello_family();
      $output .= $this->hello_me();
      return $output;
   }
}

 class SmallExample extends Example{
    
    function callProtectedMethod(){
       return $this->hello_family();
    }
 }

//------------------------------------------------------------------
//setter i getter metode
class SetterGetterExample{
   private $a = 1;
   
   public function get_a(){
      //npr. ovdje je mogla ići metoda "log_users_ip_addres" ako bi željeli ovo koristiti kao prijavu
      return $this->a;
   }
   
   public function set_a($value){
       $this->a = $value;
   }
}
//-----------------------------------------------------------------
//Statičke metode i atributi
class Student {
   static $total_Students=0;
   static public function add_student(){
      Student::$total_Students++; 
   }
   static function welcome_students($var="Hello"){
      return "{$var} studenti.";
   }
}


class One{
  static $varijabla;
}
class Two extends One{}
class Three extends One{}

//--------------------------------------------------------------
//Referenciranje parent klase
class A {
   public function hello(){
      echo "<br>Hello<br>";
  }
}

class B extends A {
  public function instance_test(){
     $this->hello();           //ovo se može i drugačije zapisati,rezultat je isti
     parent::hello();         //direktno se referencira metoda u klasi A
     //imamo pristup i prema $this i prema parent::
     //Znači, možemo imati dvije različite metode. Metoda hello u klasi B će override-ati, ali se može iskoristiti i
     //nepromjenjena parent koja je definirana u A klasi
     
  }
   public function hello(){
        echo "Pozvao sam hello metodu u B klasi i sad ću pozvati metodu hello u A klasi";
        parent::hello();
        echo "nakon poziva metode hello u A klasi";
     }
}

//-----------------------------------------------------------------
// constructor i destructor
 class Table {
   public $legs;
   static public $total_tables = 0;
   function __construct($leg_count = 3){
      $this->legs = $leg_count;
      Table::$total_tables++;
   }
   function __destruct(){
      Table::$total_tables--;
   
   }
 }

class Menu {
  var $path;
  
  public function displayMenu() {
	$menu = "";  
    $folders = array_slice(scandir($this->path), 2);
    foreach ($folders as $folder) {
       $menu .= "<div class=\"mdropdown\">";
       $menu .= "<button class=\"mdropbtn\">" . $folder ." <i class=\"fa fa-caret-down\"></i></button>";	   
	   $menu .= $this->subMenu($folder);
	   $menu .= "</div>";
    }  
    return $menu;
  }
  private function subMenu($folder) {
    $files = array_slice(scandir($this->path . "/" . $folder), 2);
	$submenu = "<div class=\"mdropdown-content\">";
    foreach ($files as $file) {
	      $submenu .= "<a href=\"". "/zadaci/pages" ."/". $folder . "/" . $file ."\">" . $file . "</a>";
	   }
	   $submenu .= "</div>";
    return $submenu;
  }
} 


?>

