<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php"; ?>
<div class="mnavbar">
<?php
$pages = "pages";
//echo createMenu($_SERVER['DOCUMENT_ROOT'] . "/zadaci/pages");
$menu = new Menu();
$menu->path = $_SERVER['DOCUMENT_ROOT'] . "/zadaci/pages";
echo $menu->displayMenu();


if (!function_exists('createMenu')) {
	function createMenu($path){
	   $folders = array_slice(scandir($path), 2);
	   $menu = "";
	   foreach ($folders as $folder) {
		   $menu .= "<div class=\"mdropdown\">";
	       $menu .= "<button class=\"mdropbtn\">" . $folder ." <i class=\"fa fa-caret-down\"></i></button>";
		   $files = array_slice(scandir($path . "/" . $folder), 2);
		   
		   $menu .= "<div class=\"mdropdown-content\">";
		   foreach ($files as $file) {
		      $menu .= "<a href=\"". "/zadaci/pages" ."/". $folder . "/" . $file ."\">" . $file . "</a>";
		   }
		   $menu .= "</div></div>";
	   }
	   return $menu;
	}
} 
?>
</div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-6">
