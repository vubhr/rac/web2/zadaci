<?php
if(isset($_GET["agreed"])) {
	setcookie("agreed", "true");
} 
?>
<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php";?>

<h1>Stranica 
<?php echo f_int2string($_SERVER['PHP_SELF'])?>
</h1>
<p> Nalazimo se na stranici 
<?php echo f_int2string($_SERVER['PHP_SELF'])?>
</p>

<script>
$(document).ready(function(){   
    setTimeout(function () {
        $("#cookieConsent").fadeIn(200);
     }, 1000);
    $("#closeCookieConsent, .cookieConsentOK").click(function() {
        $("#cookieConsent").fadeOut(200);
    }); 
}); 
</script>
<?php
if(!isset($_COOKIE["agreed"]) && !isset($_GET["agreed"])){
    include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/cookie.php";
} 
?>

</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>
