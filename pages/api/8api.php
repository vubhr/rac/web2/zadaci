<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php";?>

<h1>Stranica 
<?php echo f_int2string($_SERVER['PHP_SELF'])?>
</h1>
<p> Nalazimo se na stranici 
<?php echo f_int2string($_SERVER['PHP_SELF'])?>
</p>

<?php
$a = array('Marko','Marković','mmarkovic@vub.hr');

echo "Izlaz: ",  json_encode($a), "<br><br>";

$b = array();

echo "Prazno polje, izlaz je polje: ", json_encode($b), "<br>";
echo "Prazno array, Izlaz je objekt: ", json_encode($b, JSON_FORCE_OBJECT), "<br><br>";

$c = array(array(1,2,3));

echo "Obično polje, izlaz je polje: ", json_encode($c), "<br>";
echo "Obično polje, izalz je objekt: ", json_encode($c, JSON_FORCE_OBJECT), "<br><br>";

$d = array('ime' => 'Marko', 'prezime' => 'Marković');

echo "Asocijativno polje je uvijek objekt: ", json_encode($d), "<br>";
echo "Asocijativno polje je uvijek objekt: ", json_encode($d, JSON_FORCE_OBJECT), "<br><br>";
?>


</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>