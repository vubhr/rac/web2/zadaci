<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php";?>

<h1>Stranica 
<?php echo f_int2string($_SERVER['PHP_SELF'])?>
</h1>
<p> Nalazimo se na stranici 
<?php echo f_int2string($_SERVER['PHP_SELF'])?>
</p>
<?php
$json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';

var_dump(json_decode($json));
echo "<br><br>";
var_dump(json_decode($json, true));
echo "<br><br>";

$json = '{"broj": 12345}';
$obj = json_decode($json);
echo "broj je:" . $obj->broj;

// Sljedeći su ispravni JavaScript koda ali ne i JSON
// i key i value ako je string mora biti zatvoren s "" ne ''
// korištenjem '' JSON postaje invalidan 
$bad_json = "{ 'bar': 'baz' }";
echo "<br><br>";
json_decode($bad_json); // null
echo "<br><br>";

// the name must be enclosed in double quotes
$bad_json = '{ bar: "baz" }';
json_decode($bad_json); // null
echo "<br><br>";

// trailing commas are not allowed
$bad_json = '{ bar: "baz", }';
json_decode($bad_json); // null
echo "<br><br>";


?>

</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>