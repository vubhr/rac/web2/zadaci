<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php"; ?>


    <h1>Stranica 04</h1>
    <p> Nalazimo se na stranici 04</p>
<?php 
   //echo getcwd();
   $file1 = "./files/datoteka01.txt";
   $file2 = "./files/datoteka02.txt";

   chdir("../..");
 
   if(file_exists($file1)){
      $handle = fopen($file1, "r") or die("Ne mogu otvoriti datoteku! Molimo javite se službi za informatiku.");
	  $numBytes = 20;
	  
	  $tekst = fread($handle, $numBytes);
	  
	  echo "Pročitao sam $numBytes byte-a iz datoteke $file1 <br>";
	  echo "Tekst = <i>$tekst</i> <br><br>";
	  echo "Sad ću pročitati cijelu datoteku<br>";
	  
	  //vraćam pointer na početak, inače nastavlja od tamo gdje je stao
	  rewind($handle); 
	  
	  $tekst = fread($handle, filesize($file1));
	  echo "veličina datoteke je " . filesize($file1) . "<br>";
	  echo "Tekst cijele datoteke = <i>$tekst</i> <br><br>";
	  
	  fclose($handle);
	  
	  echo "Ponovno čitanje cijele datoteke pomoću <b>readfile</b> ona ispisuje sadržaj direktno na stranicu funkcije<br> ";
	  echo "<i>";
	  readfile($file1) or die ("Ne mogu otvoriti datoteku! Molimo javite se službi za informatiku.");
	  echo "</i>";
	  echo "<br><br>";
	  echo "Zato je možda bolja funkcija <b>file_get_contents</b> koja vraća rezultat u string<br>";
	  $tekst = file_get_contents($file1);
	  echo "Tekst pročitan pomoću  file_get_contents funkcije <i>$tekst</i>";
   }else{
      echo "Datoteka <b> $file1 </b> ne psotoji";
   }
   if(file_exists($file2)){
      echo "<br><br>";
	  echo "Za čitanje datoteke i vraćanje u polje liniju po liniju koristimo funkciju file<br>";
	  $polje = file($file2) or die("Ne mogu otvoriti datoteku! Molimo javite se službi za informatiku.");
	  echo "<pre>";
	  print_r($polje);
	  echo "</pre>";
      
	  foreach($polje as $linija){
        echo "<i>" . $linija . "</i><br>";
      }
   }else{   
	  echo "Datoteka <b> $file2 </b> ne psotoji"; 
   }
   
?>	

</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>
