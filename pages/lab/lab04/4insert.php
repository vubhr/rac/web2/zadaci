<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php"; ?>
<?php

$korisnik=['ime' => "Mario",
           'prezime' => "Kušević",
           'OIB'  =>  65822145815,
           'email' => "mkusevic@vub.hr",
           'godiste' => 1999];
    
function getInsert($array, $tableName){
    $keyArray = [];
    $valueArray = [];
    foreach($array as $key => $value){
        $keyArray[] = $key;
    }
    foreach($array as $value){
        if( is_string($value) ){
            $valueArray[] =  '"' . $value . '"';
        }
        else $valueArray[] = $value;
    }        
    return 'insert into ' . $tableName . ' (' . implode(", ", $keyArray) . 
    ') values (' . implode(", ", $valueArray) . ');';
}

echo getInsert($korisnik, 'korisnici');
?>

    </div>
    <div class="col-xs-6">
        <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
    </div>
</div>
</div>

<?php include $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>