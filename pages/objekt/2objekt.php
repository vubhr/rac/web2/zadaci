<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php"; 
  $prored = "<br><br>"; 
  //koje sve metode sadrži klasa Person
  $methods = get_class_methods('Person');
  echo "<b>" . "Metode klase Person" . "</b><br>";
  foreach($methods as $method){
      echo "Metoda " . $method . "<br>";
  }
  
  //koje sve atribute sadrži klasa
  $vars = get_class_vars('Person');
  foreach ($vars as $var => $value){
     echo "<br>Atributi: " ."<b>" . $var . "</b>" . " inicijalna vrijednost = ". $value; 
  }
  
  
  //boolean provjera da li klasa postoji 
  $klasa = "Person";
  echo $prored;
  echo "Da li je definirana klasa $klasa?<br>";
  
  if(class_exists($klasa)){
     echo "$klasa je definirana";
  }else{
     echo "$klasa nije definirana";
  } 
  
  
  //boolean provjera da li metoda unutar klase postoji
  echo $prored;
  $metoda = "say_hello";
  echo "Da li postoji metoda $metoda unutar klase $klasa";
  
  if (method_exists($klasa, $metoda)){
     echo "<br>Metoda $metoda postoji";
  }else{
     echo "<br>Metoda $metoda ne postoji";
  };
  

  //boolean provjera da li postoji atribut
  echo $prored;
  $atribut = "first_name";
  
  echo  property_exists($klasa, $atribut) ? '<br>Atribut $atribut postoji' : '<br>Atribut $atribut ne postiji';
  

?>

</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>
