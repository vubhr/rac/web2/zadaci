<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php";
$prored = "<br><br>";

  //instanciranje, kreiranje objekta
  $person = new Person();
  
  //pozivanje metode 
  $person->say_hello();
  $person->hello();
  
  echo $prored;
  //kad se poziva atribut tada se ne koristi $ jer bi to značilo dinamičku varijablu ono $$var
  echo "<br>Atribut iz instance person arm_count = ". $person->arm_count;
  
  echo $prored;
  
  //postavljanje vrijednosti atributa
  $person->arm_count = 2;
  $person->first_name = 'Željko';
  $person->last_name = 'Kovačić';
  
  //instanciranje novog objekta, kreiranje objekta $new_person
  $new_person = new Person();
  $new_person->arm_count = 2;
  $new_person->first_name = 'Marijan';
  $new_person->last_name = 'Mišković';
 
  //poziv atributa 
  echo "<b>Dohvaćanje atributa iz objekata</b>";
  echo "<br> Ime person " . $person->first_name;  
  echo "<br> Ime new_person " . $new_person->first_name; 
  
  //poziv metoda
  echo $prored;
  echo "<b>Pozivanje metoda iz objekata</b>";  
  echo "<br> Ime i prezime new_person " . $new_person->full_name(); 
  echo "<br> Ime i prezime person " . $person->full_name(); 
  

?>
</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>
