<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php";
$prored = "<br><br>";

 $example = new Example();
 echo "public a = ". $example->a ."<br>";
 //echo "protected b = ". $example->b ."<br>";  // ne može se vrtiti ovaj kod jer automatski ispod toga ništa više ne radi
 //echo "private c = ". $example->c ."<br>";
 
 //poziv public metode iz klase
 echo "<br> Poziv public metode iz klase --> ".$example->hello_everyone();
 
 echo $prored;
 
 //poziv public klase koja poziva public, private i protected atribute
 echo "poziv public klase koja poziva public, private i protected atribute<br>";
 $example->show_abc();
 
 echo $prored;
 //instanciram klasu SmallExample koja je subklasa od Example i onda imam pristup protected klasama i atributima
 $smallExample = new SmallExample();
 echo "<br> Poziv protected metode iz subClass-e ". $smallExample->callProtectedMethod();
 
 
// echo "<br> ".$example->hello_family();
 echo "<br> ".$example->hello_me();
 echo "<br> ".$example->hello();
  

?>

</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>
