<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/include.php";
  $prored = "<br><br>";

  $bmw = new Auto();
  $tesla = new Električni();
  
  echo "bmw ima " . $bmw->kotači . " kotača<br>";
  echo "bmw ima " . $bmw->vrata . " vrata<br>";
  echo "bmw ima " . $bmw->ukupanBroj() . " kotača i vrata<br>";
  echo $prored;
  echo "tesla ima " . $tesla->kotači . " kotača<br>";
  echo "tesla ima " . $tesla->vrata . " vrata<br>";
  echo "tesla ima " . $tesla->ukupanBroj() . " kotača, vrata i baterija<br>";
  echo $prored;
  
  //kako programski pronaći međuovisnost između klasa
  echo "Parent klasa od Auto je: " . get_parent_class('Auto');
  echo $prored;
  echo "Parent klasa od Električni je: " . get_parent_class('Električni');
  echo $prored;
  
  echo "Da li je Auto subklasa od Auto<br>";
  echo is_subclass_of('Auto', 'Auto') ? 'true' : 'false';
  echo $prored;
  echo "Da li je Električni subklasa od Auto<br>";
  echo is_subclass_of('Električni', 'Auto') ? 'true' : 'false';
  echo $prored;
  echo "Da li je Auto subklasa od Električni<br>";
  echo is_subclass_of('Auto', 'Električni') ? 'true' : 'false';
  echo $prored;
  

?>

</div>
        <div class="col-xs-6">
            <?php echo "<b>" . __FILE__ . "</b><br>"; highlight_file(__FILE__);?>
        </div>
    </div>
</div>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . "/zadaci/common/footer.php"; ?>
